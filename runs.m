pop = 20;
gen = 20;
no_iter = 300;

multi_bestobj = zeros(pop);
for i = 1:no_iter
    list_bestobj = monte(pop,gen);
    multi_bestobj(:,:,i) = list_bestobj;    
end

average = mean(multi_bestobj,3);
std_dev = std(multi_bestobj,0,3);

sortAverage = sort(average(:));
top5_average = zeros(1,5);
top5_indices = zeros(5,2);
top5_std = zeros(1,5);
for i = 1:5
    top5_average(1,i) = sortAverage(i);
    [row_pop, row_gen] = find(average==sortAverage(i));
    top5_indices(i,:) = [row_pop, row_gen];
    top5_std(1,i) = std_dev(row_pop, row_gen);
end

x = 1:pop;
y = 1:gen;
figure
surf(x,y,average)
colorbar

figure
errorbar(1:5, top5_average, top5_std)
