slice_average = average(3:20,3:20);
sortAverage = sort(slice_average);

top10_average = zeros(1,10);
top10_indices = zeros(10,2);
top10_std = zeros(1,10);
for i = 1:10
    top10_average(1,i) = sortAverage(i);
    [row_pop, row_gen] = find(average==top10_average(1,i));
    top10_indices(i,:) = [row_pop, row_gen];
    top10_std(1,i) = std_dev(row_pop, row_gen);
end

m = 1:18;
n = 1:18;
figure
surf(m,n,slice_average)

figure
errorbar(1:10,top10_average,top10_std)
