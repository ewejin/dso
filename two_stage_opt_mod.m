function [bestobj4, bestvar4] = two_stage_opt(m)
LB = [0,0,0,0.5,0.5,0.5,0.5];
UB = [1,1,1,0.5,0.5,0.5,0.5];    
%1st 3 variables
GAOptions=gaoptimset('PopulationSize',4,'Generations',2);
[bestvar,bestobj,history,eval_count]=ga(@Wing_Design,3,[],[],[],[],LB,UB,[],GAOptions);
eval_left = m - eval_count;

options = optimset('MaxFunEvals',eval_left);
[bestvar2,bestobj2]=fminsearchcon(@Wing_Design,bestvar,LB,UB,[],[],[],options);

%Next 3 variables
GAOptions = gaoptimset('PopulationSize',8,'Generations',3);
[bestvar3,bestobj3,history,eval_count]=ga(@Wing_Design,7,[],[],[],[],[bestvar2(1),bestvar2(2),bestvar2(3),0,0,0,0],[bestvar2(1),bestvar2(2),bestvar2(3),1,1,1,1],[],GAOptions);
eval_left = (120-m) - eval_count;
        
options = optimset('MaxFunEvals',eval_left);
[bestvar4,bestobj4]=fminsearchcon(@Wing_Design,bestvar3,[bestvar2(1),bestvar2(2),bestvar2(3),0,0,0,0],[bestvar2(1),bestvar2(2),bestvar2(3),1,1,1,1],[],[],[],options);
