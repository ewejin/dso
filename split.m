pop = 10;
gen = 10;

list_bestobj = zeros(pop);
lowest_eval = 20;
highest_eval = 100;
% First limits
LB = [0,0,0,0.5,0.5,0.5];
UB = [1,1,1,0.5,0.5,0.5];
%split_bestobj = zeros(80);

for m = lowest_eval:highest_eval;
    for i = 1:pop
        for j = 1:gen
            
            %1st 3 variables
            GAOptions=gaoptimset('PopulationSize',i,'Generations',j);
            [bestvar,bestobj,history,eval_count]=ga(@Test_Function3,3,[],[],[],[],LB,UB,[],GAOptions);
            eval_left = m - eval_count;
            if eval_left <= 0
                break
            else
                options = optimset('MaxFunEvals',eval_left);
                [bestvar2,bestobj2]=fminsearchcon(@Test_Function3,bestvar,LB,UB,[],[],[],options);
            end
            %Next 3 variables
            GAOptions = gaoptimset('PopulationSize',i,'Generations',j);
            [bestvar3,bestobj3,history,eval_count]=ga(@Test_Function3,6,[],[],[],[],[bestvar2(1),bestvar2(2),bestvar2(3),0,0,0],[bestvar2(1),bestvar2(2),bestvar2(3),1,1,1],[],GAOptions);
            eval_left = (120-m) - eval_count;
            if eval_left <= 0
                break
            else
                options = optimset('MaxFunEvals',eval_left);
                [bestvar4,bestobj4]=fminsearchcon(@Test_Function3,bestvar3,[bestvar2(1),bestvar2(2),bestvar2(3),0,0,0],[bestvar2(1),bestvar2(2),bestvar2(3),1,1,1],[],[],[],options);
                opt_var = bestvar4;
                list_bestobj(i,j) = bestobj4;
            end
        end
    end
    min_list = min(list_bestobj(:));
    split_bestobj(m,120-m) = min_list;
end

min_split = min(split_bestobj(:));
[row, col] = find(split_bestobj==min_split);
    