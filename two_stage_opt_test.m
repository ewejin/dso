function [list_bestobj, list_bestvar] = two_stage_opt_test(pop, gen, m)
list_bestobj = ones(pop+1);
list_bestvar = ones(pop+1,pop+1,6);
LB = [0,0,0,0.5,0.5,0.5];
UB = [1,1,1,0.5,0.5,0.5];
for i = pop-2:pop+1
    for j = gen-1:gen+2
       
        %1st 3 variables
        GAOptions=gaoptimset('PopulationSize',i,'Generations',j);
        [bestvar,bestobj,history,eval_count]=ga(@Test_Function3,3,[],[],[],[],LB,UB,[],GAOptions);
        eval_left = m - eval_count;
        if eval_left <= 0
            formatSpec = 'eval_left is neg';
            sprintf(formatSpec)
            list_bestvar(i,j,:) = NaN;
            list_bestobj(i,j) = NaN;
            break
        else
            options = optimset('MaxFunEvals',eval_left);
            [bestvar2,bestobj2]=fminsearchcon(@Test_Function3,bestvar,LB,UB,[],[],[],options);
        end
        %Next 3 variables
        GAOptions = gaoptimset('PopulationSize',i,'Generations',j);
        [bestvar3,bestobj3,history,eval_count]=ga(@Test_Function3,6,[],[],[],[],[bestvar2(1),bestvar2(2),bestvar2(3),0,0,0],[bestvar2(1),bestvar2(2),bestvar2(3),1,1,1],[],GAOptions);
        eval_left = (120-m) - eval_count;
        if eval_left <= 0
            break
        else
            options = optimset('MaxFunEvals',eval_left);
            [bestvar4,bestobj4]=fminsearchcon(@Test_Function3,bestvar3,[bestvar2(1),bestvar2(2),bestvar2(3),0,0,0],[bestvar2(1),bestvar2(2),bestvar2(3),1,1,1],[],[],[],options);
            list_bestvar(i,j,:) = bestvar4;
            list_bestobj(i,j) = bestobj4
        end
    end
end
end