function [ list_bestobj ] = monte( pop, gen )
list_bestobj = zeros(pop);
for i = 1:pop
    for j = 1:gen
    
        GAOptions=gaoptimset('PopulationSize',i,'Generations',j);
        [bestvar,bestobj,history,eval_count]=ga(@Test_Function3,6,[],[],[],[],[0,0,0,0,0,0],[1,1,1,1,1,1],[],GAOptions);
        eval_left = 120 - eval_count;
        if eval_left <= 0
            break
        else
            options = optimset('MaxFunEvals',eval_left);
            [bestvar2,bestobj2]=fminsearchcon(@Test_Function3,bestvar,[0,0,0,0,0,0],[1,1,1,1,1,1],[],[],[],options);
            list_bestobj(i,j) = bestobj2;
        end
    end
end