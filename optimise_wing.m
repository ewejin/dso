GAOptions=gaoptimset('PopulationSize',9,'Generations',3);
[bestvar,bestobj,history,eval_count]=ga(@Wing_Design,7,[],[],[],[],[0,0,0,0,0,0,0],[1,1,1,1,1,1,1],[],GAOptions);
eval_left = 120 - eval_count;
options = optimset('MaxFunEvals',eval_left);
[bestvar2,bestobj2]=fminsearchcon(@Wing_Design,bestvar,[0,0,0,0,0,0,0],[1,1,1,1,1,1,1],[],[],[],options);
variables = bestvar2;
func_obj = bestobj2;

% matlab.io.saveVariablesToScript(results)